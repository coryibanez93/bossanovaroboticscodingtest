#include "BigNum.h"
#include <iostream>
#include <iomanip>
#define UINT64_T_MAX 18446744073709551615U

//Constructor for initializing BigNum variable using setVals function
BigNum::BigNum(uint64_t a, uint64_t b){
    setVals(a,b);
}

//Sets values for BigNum object where a=lower 64 bits and b=upper 64 bits
void BigNum::setVals(uint64_t a,uint64_t b){
    lower_ = a;
    upper_ = b;
}

//Prints BigNums value in the form of a 32 digit hexadecimal number beginning with "0x"
void BigNum::printBigNumHex(){
    cout << "0x";
    cout << hex << upper_;
    cout << std::setw(16) << setfill('0') << hex << lower_; //fill 16 bit hex value with leading zeros
    cout << dec;
}

//Prints the lower 64 bits and upper 64 bits of the big num as their decimal representation
void BigNum::printBigNum(){
    cout << ", Upper: " << upper_ << "Lower: " << lower_;
}

//converts a=uint64_t to a BigNum and returns a BigNum object using value of a as lower 64 bits
BigNum BigNum::uint64_tToBigNum(uint64_t a){
    BigNum bigNum(a,0);
    return bigNum;
}

//Adds two BigNum objects together and returns resulting BigNum
//Does not support overflow when moving beyond upper bounds of the upper 64 bits of the BigNum
BigNum addBigNums(BigNum input1, BigNum input2){
    BigNum bigNum(0,0);
    uint64_t lower1 = input1.lower();
    uint64_t lower2 = input2.lower();
    uint64_t upper1 = input1.upper();
    uint64_t upper2 = input2.upper();
    uint64_t resultLower = 0;
    uint64_t resultUpper = 0;

    if(lower1 > UINT64_T_MAX - lower2){
        resultLower = lower1 - (UINT64_T_MAX - lower2) - 1;
        resultUpper++;
    }
    else{
        resultLower = lower1 + lower2;
    }
    resultUpper += upper1;
    resultUpper += upper2;
    bigNum.setVals(resultLower, resultUpper);

    return bigNum;
}
