#ifndef BIGNUM_H_INCLUDED
#define BIGNUM_H_INCLUDED
#include <math.h>
#include <stdint.h>

using namespace std;

//BigNum class
//Object consists of two uint64_t values representing a theoretical uint128_t
class BigNum{
    private:
        uint64_t upper_, lower_;

    public:
        BigNum (uint64_t, uint64_t);
        void setVals(uint64_t,uint64_t);

        BigNum uint64_tToBigNum(uint64_t);
        BigNum addBigNums(BigNum, BigNum);

        void printBigNum();
        void printBigNumHex();

        uint64_t lower() const { return lower_; }
        uint64_t upper() const { return upper_; }
};

BigNum addBigNums(BigNum input1, BigNum input2);


#endif // BIGNUM_H_INCLUDED
