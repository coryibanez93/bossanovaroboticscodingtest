#include "BigNum.h"
#include <math.h>
#include <stdint.h>
#include <iostream>



//fibIterative
//Input: int n = {0,100} - desired value of Fibonacci sequence to be calculated
//Returns: BigNum object defining the current f(n) value in the Fibonacci sequence
//Expected run time of O(n)
BigNum fibIterative(int n){
    //Vars needed for Fibonacci sequence formula f(n) = f(n-1) + f(n-2)
    BigNum fibResult(0,0);
    BigNum fibResultPrev(0,0);
    BigNum fibResultNew(0,0);

    //Iterate up to n, calculating values at each point
    for(int i = 0; i <= n; i++){
        if(i == 1){
            fibResult.setVals(1,0);
        }
        else{
            fibResultNew = addBigNums(fibResultPrev,fibResult); //Calculate next value in Fibonacci sequence
            fibResultPrev  = fibResult;                         //Assign next n-2 value
            fibResult = fibResultNew;                           //Assign current Fibonacci value and next n-1 value
        }
    }

    //return calculated result of f(n) as a BigNum object
    return fibResult;
}

//fibRecursiveFast
//Input: int n = {0,100} - desired value of Fibonacci sequence to be calculated
//Returns: BigNum object defining the current f(n) value in the Fibonacci sequence
//Expected run time of O(n^2)
BigNum fibRecursive(int n){
    //Define seed values for the fib sequence f(1)=1, f(0)=0
    if(n == 1){
        BigNum BigNumOne(1,0);
        return BigNumOne;
    }
    else if(n == 0){
        BigNum BigNumZero(0,0);
        return BigNumZero;
    }
    else{
        return addBigNums(fibRecursive(n-1), fibRecursive(n-2)); //Recursively call fibRecursive to calculate the f(n-1) value and f(n-2) value
    }
}

//fibRecursiveFast
//Input: int n = {0,100} - desired value of Fibonacci sequence to be calculated
//Returns: a pair of BigNum objects defining the current f(n) value in the Fibonacci sequence and the previous value f(n-1)
//Expected run time of O(n)
pair<BigNum, BigNum> fibRecursiveFast(int n){
    //Define seed values for the fib sequence f(1)=1, f(0)=0
    if(n == 1){
        BigNum BigNumCurr(1,0);
        BigNum BigNumPrev(0,0);
        pair<BigNum, BigNum> BigNumPair(BigNumCurr, BigNumPrev);
        return BigNumPair;
    }
    else if(n == 0){
        BigNum BigNumCurr(0,0);
        BigNum BigNumPrev(0,0);
        pair<BigNum, BigNum> BigNumPair(BigNumCurr, BigNumPrev);
        return BigNumPair;
    }
    else{
        pair<BigNum, BigNum> BigNumPrevPair = fibRecursiveFast(n-1);
        pair<BigNum, BigNum> BigNumPair(addBigNums(BigNumPrevPair.first, BigNumPrevPair.second), BigNumPrevPair.first);
        return BigNumPair;
    }

}

//fibRecursiveCaller
//Caller function to handle return value of fibRecursiveFast
//Input: int n = {0,100} - desired value of Fibonacci sequence to be calculated
//Returns: Single BigNum value indicating Fibonacci(n) instead of the pair of BigNums as is returned by fibRecursiveFast
BigNum fibRecursiveCaller(int n){
    //call fibRecursiveFast to calculate f(n)
    pair<BigNum, BigNum> fibBigNumPair = fibRecursiveFast(n);

    //return only the first element of the pair returned from fibRecursiveFast which is the desired value of Fibonacci(n)
    return fibBigNumPair.first;
}
