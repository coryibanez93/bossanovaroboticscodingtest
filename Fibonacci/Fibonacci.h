#ifndef FIBONACCI_H_INCLUDED
#define FIBONACCI_H_INCLUDED
#include "BigNum.h"
#include <math.h>
#include <stdint.h>
#include <iostream>


using namespace std;

//fibIterative
//Input: int n = {0,100} - desired value of Fibonacci sequence to be calculated
//Returns: BigNum object defining the current f(n) value in the Fibonacci sequence
//Expected run time of O(n)
BigNum fibIterative(int n);

//fibRecursiveFast
//Input: int n = {0,100} - desired value of Fibonacci sequence to be calculated
//Returns: BigNum object defining the current f(n) value in the Fibonacci sequence
//Expected run time of O(n^2)
BigNum fibRecursive(int n);

//fibRecursiveFast
//Input: int n = {0,100} - desired value of Fibonacci sequence to be calculated
//Returns: a pair of BigNum objects defining the current f(n) value in the Fibonacci sequence and the previous value f(n-1)
//Expected run time of O(n)
pair<BigNum, BigNum> fibRecursiveFast(int n);

//fibRecursiveCaller
//Caller function to handle return value of fibRecursiveFast
//Input: int n = {0,100} - desired value of Fibonacci sequence to be calculated
//Returns: Single BigNum value indicating Fibonacci(n) instead of the pair of BigNums as is returned by fibRecursiveFast
BigNum fibRecursiveCaller(int n);

#endif // FIBONACCI_H_INCLUDED
