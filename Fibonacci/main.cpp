#include "Fibonacci.h"
#include <iostream>

using namespace std;

int main()
{
    BigNum fibBigNum(0,0);

    //Testing fibIterative from 0-100
    cout << "Testing fibIterative n={0-100}, output as hex" << endl;
    for(int i = 0; i <= 100; i++){
        cout << i << ": ";
        fibBigNum = fibIterative(i);
        fibBigNum.printBigNumHex();
        cout << endl;
    }

    //Testing fibRecursiveCaller from 0-100
    cout << "Testing fibRecursiveCaller which calls fibRecursiveFast n={0-100}, output as hex" << endl;
    for(int i = 0; i <= 100; i++){
        cout << i << ": ";
        fibBigNum = fibRecursiveCaller(i);
        fibBigNum.printBigNumHex();
        cout << endl;
    }

    return 0;
}
